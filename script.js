fetch('https://jsonplaceholder.typicode.com/todos',{
  method:'GET'
})
  .then(response => response.json())
  .then(json => console.log(json))


fetch('https://jsonplaceholder.typicode.com/todos')
.then(res => res.json())
.then(data => {
  let list = data.map(post => {
    return post.title
  })
  console.log(list)
})


fetch('https://jsonplaceholder.typicode.com/todos/2',{
  method:'GET'
})
  .then(response => response.json())
  .then(json => console.log(json))



fetch('https://jsonplaceholder.typicode.com/todos/2',{
  method:'GET'
})
  .then(response => response.json())
  .then(json => console.log(`Title: ${json.title} and Status/Completed: ${json.completed}`))



fetch('https://jsonplaceholder.typicode.com/todos', {
  method: 'POST',
  headers: {
    'content-type': 'application/json'
  },
  body: JSON.stringify({
    userId: 1,
    id: 1,
    title: 'Hello',
    completed: 'true'
  })
})
.then(res => res.json())
.then(data => console.log(data))


fetch('https://jsonplaceholder.typicode.com/todos/1', {
  method: 'PUT',
  headers: {
    'content-type': 'application/json'
  },
  body: JSON.stringify({
    title: 'Updated Title',
    description: 'New Title',
    status: 'available',
    dateCompleted: 2002,
    userId: 2
  })
})
.then(res => res.json())
.then(data => console.log(data))


fetch('https://jsonplaceholder.typicode.com/todos/1', {
  method: 'PATCH',
  headers: {
    'content-type': 'application/json'
  },
  body: JSON.stringify({
    title: 'Patched Title',
  })
})


fetch('https://jsonplaceholder.typicode.com/todos/1', {
  method: 'PATCH',
  headers: {
    'content-type': 'application/json'
  },
  body: JSON.stringify({
    status: 'complete',
    dateCompleted: 2002
  })
})

fetch('https://jsonplaceholder.typicode.com/todos/3', {
  method: 'DELETE',
})

.then(res => res.json())
.then(data => console.log(data))